package ui;

import java.io.*;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;


public class Application {

	public static void main(String[] args) throws IOException {

		CLConnection cCon = new CLConnection();

		BufferedReader ConsoleInput = new BufferedReader(new InputStreamReader(System.in));
		boolean quit = false;
		// initialize logger
		//Logger mylogger = Logger.getLogger();
		String logDir = "logging/client.log";
		String pattern = "%d{ISO8601} %-5p [%t] %c: %m%n";

		PatternLayout pLayout = new PatternLayout(pattern);
		FileAppender fa = new FileAppender(pLayout, logDir, true );
		//mylogger.addAppender(fa);



		do {
			System.out.println("Echo Client>");
			String input = ConsoleInput.readLine();

			String[] tokens = input.trim().split("\\s+");

			switch (tokens[0]) {
			case "connect":
				if (cCon.connect(tokens)) {
					System.out.printf("You are now connected to %s:%s\n", tokens[1], tokens[2]);
				} else {
					System.out.println("Failed to connect");
				}
				break;
			case "disconnect":
				if (!cCon.disconnect()) {
					System.out.println("You are now disconnected from the server");
				} else {
					System.out.println("Could not disconnect form server");
				}
				break;
			case "send":
				cCon.send(tokens);
				break;
			case "logLevel":
				cCon.logLevel(tokens);
				break;
			case "help":
				help(tokens);
				break;
			case "quit":
				// cCon.disconnect();
				quit = true;
				System.out.println("You have exited the program");
				break;
			default:
				System.out.printf("%s is Unknown or illegal command. Try help <your command>\n", tokens[0]);
				break;
			}

		} while (!quit);
	}

	private static void help(String[] command) {

		if (command.length == 1) {
			System.out.println(
					"The application is used to connect and communicate with a server. The following commands are legal and executeable\n"
							+ "Connect<server,port>, disconnect, send <message>, logLevel <level>, quit");
		} else if (command.length > 2) {
			System.out.println("Unknown commands");
		} else {
			switch (command[1]) {
			case "connect":
				System.out.println(
						"Connect <host> <port>. Hostname or IP address of the echo server. The port of the echo service on the respective server.");
				break;
			case "disconnect":
				System.out.println("Disconnect from a connected server");
				break;
			case "send":
				System.out.println(
						"Send <message>. Sends a text message to the echo server according to the communication protocol.");
				break;
			case "logLevel":
				System.out.println("logLevel <level> \n "
						+ "One of the following log4j log levels:(ALL | DEBUG | INFO | WARN | ERROR | FATAL | OFF). Sets the logger to the specified log level");
				break;
			case "help":
				System.out.println(
						"The application is used to connect and communicate with a server. The following commands are legal and executeable\n"
								+ "Connect<server,port>, disconnect, send <message>, logLevel <level>, quit");
				break;
			case "quit":
				System.out.println("Tears down the active connection to the server and exits the program execution.");
				break;
			default:
				System.out.println("Unknown or illegal command");
				break;
			}
		}

	}
}
