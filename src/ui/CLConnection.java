package ui;

import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.io.*;
import org.apache.log4j.*;

public class CLConnection {
	private static Socket conn;
	private InputStream in;
	private boolean connected = false;
	private OutputStream out;

	public boolean connect(String[] string) {
		// TODO Auto-generated method stub
		try {
			conn = new Socket(string[1], Integer.parseInt(string[2]));
			out = conn.getOutputStream();
			in = conn.getInputStream();
			connected = true;
			// this.send();
			return connected;
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public boolean disconnect() {
		// TODO Auto-generated method stub
		try {
			out.close();
			in.close();
			conn.close();
			connected = false;
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connected;

	}

	public void send(String[] tokens) {

		try {
			
			// BufferedReader in = new BufferedReader(new
			// InputStreamReader(conn.getInputStream()));

			int i = 0;
			while (i < tokens.length - 1 && tokens.length > 0) {
				out.write(tokens[i + 1].getBytes());
				out.flush();
				i++;
			}
			
			byte[] recvByte = new byte[1024];
			int read;
			while ((read = in.read(recvByte)) != -1) {
				String output = new String(recvByte, StandardCharsets.UTF_8);
				System.out.print(output);
				System.out.flush();
				if (recvByte[read - 1] == 13)
					break;
			}

		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// in.close(); //Matt: Set it up on main ?

		}
		;

	}

	public void logLevel(String[] string) {
		// TODO Auto-generated method stub
		// mylogger.info(string);

	}

}