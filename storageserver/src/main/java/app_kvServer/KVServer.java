package app_kvServer;

import java.io.IOException;

import de.designflaw.clouddb.server.Configuration;
import de.designflaw.clouddb.server.Server;

public class KVServer {
	final Configuration config;
	Server server;
	Thread serverThread;

	/**
	 * Start KV Server at given port
	 * 
	 * @param port
	 *            given port for storage server to operate
	 * @param cacheSize
	 *            specifies how many key-value pairs the server is allowed to
	 *            keep in-memory
	 * @param strategy
	 *            specifies the cache replacement strategy in case the cache is
	 *            full and there is a GET- or PUT-request on a key that is
	 *            currently not contained in the cache. Options are "FIFO",
	 *            "LRU", and "LFU".
	 */
	public KVServer(final int port, final int cacheSize, final String strategy) {
		config = new Configuration() {
			@Override
			public int getPort() {
				return port;
			}

			@Override
			public int getCacheSize() {
				return cacheSize;
			}

			@Override
			public Configuration.CacheStrategy getCacheStrategy() {
				if (strategy.equals("FIFO"))
					return Configuration.CacheStrategy.FIFO;
				if (strategy.equals("LFU"))
					return Configuration.CacheStrategy.LFU;
				if (strategy.equals("LRU"))
					return Configuration.CacheStrategy.LRU;
				return null;
			}
		};
		server = Server.init(config);
		serverThread = new Thread(server);
		serverThread.start();
	}

	public void stop() throws IOException {
		server.getSocket().close();
		serverThread.interrupt();
	}
}
