package client;

import java.io.IOException;

import common.messages.KVMessage;
import de.designflaw.clouddb.client.RemoteStorage;
import de.designflaw.clouddb.storage.Optional;
import de.designflaw.clouddb.storage.StorageException;

/**
 * Just a wrapper
 * 
 * @author flo
 *
 */
public class KVStore implements KVCommInterface {
	final String address;
	final int port;
	RemoteStorage client;

	/**
	 * Initialize KVStore with address and port of KVServer
	 * 
	 * @param address
	 *            the address of the KVServer
	 * @param port
	 *            the port of the KVServer
	 */
	public KVStore(String address, int port) {
		this.address = address;
		this.port = port;
	}

	@Override
	public void connect() throws Exception {
		client = RemoteStorage.open(address, port);
	}

	@Override
	public void disconnect() {
		try {
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public KVMessage put(final String key, final String value) throws Exception {
		if (value.equals("null")) {
			if (client.remove(key)) {
				return new KVMessage() {

					@Override
					public String getKey() {
						return key;
					}

					@Override
					public String getValue() {
						return null;
					}

					@Override
					public StatusType getStatus() {
						return KVMessage.StatusType.DELETE_SUCCESS;
					}

				};
			} else {
				return new KVMessage() {

					@Override
					public String getKey() {
						return key;
					}

					@Override
					public String getValue() {
						return null;
					}

					@Override
					public StatusType getStatus() {
						return KVMessage.StatusType.DELETE_ERROR;
					}

				};
			}
		}
			if (client.put(key, value)) {
				return new KVMessage() {

					@Override
					public String getKey() {
						return key;
					}

					@Override
					public String getValue() {
						return value;
					}

					@Override
					public StatusType getStatus() {
						return KVMessage.StatusType.PUT_UPDATE;
					}
				};
			} else {
				return new KVMessage() {

					@Override
					public String getKey() {
						return key;
					}

					@Override
					public String getValue() {
						return value;
					}

					@Override
					public StatusType getStatus() {
						return KVMessage.StatusType.PUT_SUCCESS;
					}

				};
			}
	}

	@Override
	public KVMessage get(final String key) throws Exception {
		try {
			final Optional<String> res = client.get(key);

			if (res.present()) {
				return new KVMessage() {

					@Override
					public String getKey() {
						return key;
					}

					@Override
					public String getValue() {
						return res.get();
					}

					@Override
					public StatusType getStatus() {
						return KVMessage.StatusType.GET_SUCCESS;
					}

				};
			} else {
				return new KVMessage() {

					@Override
					public String getKey() {
						return key;
					}

					@Override
					public String getValue() {
						return "Not Found";
					}

					@Override
					public StatusType getStatus() {
						return KVMessage.StatusType.GET_ERROR;
					}
				};
			}
		} catch (final StorageException e) {
			return new KVMessage() {

				@Override
				public String getKey() {
					return key;
				}

				@Override
				public String getValue() {
					return e.getMessage();
				}

				@Override
				public StatusType getStatus() {
					return KVMessage.StatusType.GET_ERROR;
				}

			};
		}
	}

}
