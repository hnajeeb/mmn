package de.designflaw.clouddb.echoclient;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.StateCallback;

public class DisconnectCommand implements CommandLineHandler<ClientState> {
	private static Pattern regex = Pattern.compile("^disconnect\\s*$");

	private static final Logger logger = LogManager.getRootLogger();

	@Override
	public boolean tryExec(String command,
			StateCallback<ClientState> ifaceRef) {
		if (!regex.matcher(command).matches())
			return false;
		if (ifaceRef.getState().socket == null) {
			ifaceRef.getOut().println("Not connected.");
			logger.warn("Tried to disconnect without connection");
		}
		try {
			ifaceRef.getState().fwd.interrupt();
			ifaceRef.getState().socket.close();
		} catch (IOException e) {
			logger.error("I/O Error", e);
		}
		return true;
	}

}
