package de.designflaw.clouddb.echoclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class InputForwarder implements Runnable {
	Reader in;
	PrintStream out;

	public static Charset charset = StandardCharsets.ISO_8859_1;

	private static final Logger logger = LogManager.getRootLogger();

	public InputForwarder(InputStream in, PrintStream out) {
		this.in = new InputStreamReader(in, charset);
		this.out = out;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				if (in.ready())
					out.write(in.read());
				else
					Thread.sleep(1);
			}
		} catch (IOException e) {
			logger.error("I/O Error", e);
		} catch (InterruptedException e) {
			logger.info("Finished forwarding stream");
		} finally {
			out.flush();
		}
	}
}
