package de.designflaw.clouddb.echoclient;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import de.designflaw.clouddb.cli.CLIExecutable;
import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.CommandLineInterface;

public class EchoClient implements CLIExecutable {

	List<CommandLineHandler<ClientState>> handlers;
	CommandLineInterface<ClientState> cli;

	public EchoClient() {
		handlers = new ArrayList<CommandLineHandler<ClientState>>();
		setup();
	}

	private void setup() {
		handlers.add(new ConnectCommand());
		handlers.add(new DisconnectCommand());
		handlers.add(new QuitCommand());
		handlers.add(new SendCommand());
		handlers.add(new HelpCommand());
		handlers.add(new LogLevelCommand());
		cli = new CommandLineInterface<ClientState>("EchoClient> ", handlers,
				new ClientState());
	}

	public EchoClient(InputStream in, PrintStream out) {
		handlers = new ArrayList<CommandLineHandler<ClientState>>();
		setup(in, out);
	}

	private void setup(InputStream in, PrintStream out) {
		handlers.add(new ConnectCommand());
		handlers.add(new DisconnectCommand());
		handlers.add(new QuitCommand());
		handlers.add(new SendCommand());
		handlers.add(new HelpCommand());
		handlers.add(new LogLevelCommand());
		cli = new CommandLineInterface<ClientState>("EchoClient> ", handlers,
				new ClientState(), out, in);
	}

	@Override
	public void exec() {
		cli.exec();
	}

}
