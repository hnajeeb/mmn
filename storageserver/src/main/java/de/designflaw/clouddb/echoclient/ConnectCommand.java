package de.designflaw.clouddb.echoclient;

import java.io.IOException;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.StateCallback;

public class ConnectCommand implements CommandLineHandler<ClientState> {
	private static Pattern regex = Pattern
			.compile("^connect\\s+(.+)\\s+(\\d+)\\s*$");

	private static final Logger logger = LogManager.getRootLogger();

	@Override
	public boolean tryExec(String command, StateCallback<ClientState> ifaceRef) {
		ClientState st = ifaceRef.getState();
		Matcher match = regex.matcher(command);
		if (!match.matches())
			return false;
		try {
			String host = match.group(1);
			int port = Integer.parseInt(match.group(2));
			st.socket = new Socket(host, port);
			st.fwd = new Thread(new InputForwarder(st.socket.getInputStream(),
					System.out));
			st.fwd.start();
		} catch (IOException e) {
			logger.error("I/O Error", e);
		}
		return true;
	}

}
