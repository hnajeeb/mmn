package de.designflaw.clouddb.echoclient;

import java.util.regex.Pattern;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.StateCallback;

public class QuitCommand implements CommandLineHandler<ClientState> {
	private static Pattern regex = Pattern.compile("^quit\\s*$");

	@Override
	public boolean tryExec(String command, StateCallback<ClientState> ifaceRef) {
		if (!regex.matcher(command).matches())
			return false;
		ifaceRef.setExit(true);
		return true;
	}

}
