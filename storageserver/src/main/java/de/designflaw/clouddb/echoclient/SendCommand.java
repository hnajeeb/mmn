package de.designflaw.clouddb.echoclient;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.StateCallback;

public class SendCommand implements CommandLineHandler<ClientState> {
	private static Pattern regex = Pattern.compile("^send\\s(.+)\\s*$");

	public static Charset charset = StandardCharsets.ISO_8859_1;
	public static char newline = '\r';

	private static final Logger logger = LogManager.getRootLogger();

	@Override
	public boolean tryExec(String command, StateCallback<ClientState> ifaceRef) {
		Matcher match = regex.matcher(command);
		if (!match.matches())
			return false;
		if (ifaceRef.getState().socket == null) {
			ifaceRef.getOut().println("Not connected.");
			logger.warn("Tried to send message without connection");
			return true;
		}
		try {
			String sendString = match.group(1);
			byte[] msg = sendString.getBytes(charset);

			ifaceRef.getState().socket.getOutputStream().write(msg);
			ifaceRef.getState().socket.getOutputStream().write(newline);
			ifaceRef.getState().socket.getOutputStream().flush();
			logger.info("Sent " + msg.length + 1 + " bytes to "
					+ ifaceRef.getState().socket.getInetAddress().getHostName());
		} catch (IOException e) {
			logger.error("I/O error", e);
		}
		return true;
	}

}
