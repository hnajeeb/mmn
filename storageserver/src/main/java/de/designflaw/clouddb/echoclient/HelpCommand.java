package de.designflaw.clouddb.echoclient;

import java.util.regex.Pattern;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.StateCallback;

public class HelpCommand implements CommandLineHandler<ClientState> {
	private static Pattern regex = Pattern.compile("^help\\s*$");

	static String helpText = "Following commands are available:\n"
			+ "* connect <hostname> <port>\n" + "* disconnect\n" + "* help\n"
			+ "* quit\n" + "* send <message>\n";

	@Override
	public boolean tryExec(String command,
			StateCallback<ClientState> ifaceRef) {
		if (!regex.matcher(command).matches())
			return false;
		ifaceRef.getOut().println(helpText);
		return true;
	}

}
