package de.designflaw.clouddb.echoclient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.StateCallback;

public class LogLevelCommand implements CommandLineHandler<ClientState> {
	private static Pattern regex = Pattern
			.compile("^logLevel\\s+([A-Z]+)\\s*$");

	private static final Logger logger = LogManager.getRootLogger();

	@Override
	public boolean tryExec(String command, StateCallback<ClientState> ifaceRef) {
		Matcher match = regex.matcher(command);
		if (!match.matches())
			return false;
		Level logLevel = null;
		try {
			logLevel = Level.toLevel(match.group(1));
		} catch (IllegalArgumentException e) {
			logger.warn("Log level \"" + match.group(1) + "\" not recognized.");
			return true;
		}
		logger.setLevel(logLevel);
		return true;
	}
}
