package de.designflaw.clouddb.concurrency;

import java.util.HashSet;

public class KeyedLockStore<K extends Comparable<K>> implements LockStore<K> {
	HashSet<K> reads;
	HashSet<K> writes;

	public KeyedLockStore() {
		reads = new HashSet<K>();
		writes = new HashSet<K>();
	}

	@Override
	public synchronized boolean tryLockRead(K key) {
		if (writes.contains(key))
			return false;
		reads.add(key);
		return true;
	}

	@Override
	public synchronized boolean tryLockWrite(K key) {
		if (reads.contains(key))
			return false;
		if (writes.contains(key))
			return false;
		writes.add(key);
		return true;
	}

	@Override
	public void lockRead(K key) {
		if (tryLockRead(key))
			return;
		try {
			synchronized (this) {
				wait();
			}
			lockRead(key);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void lockWrite(K key) {
		if (tryLockWrite(key))
			return;
		try {
			synchronized (this) {
				wait();
			}
			lockWrite(key);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void freeLockRead(K key) {
		reads.remove(key);
		notifyAll();
	}

	@Override
	public synchronized void freeLockWrite(K key) {
		writes.remove(key);
		notifyAll();
	}

}
