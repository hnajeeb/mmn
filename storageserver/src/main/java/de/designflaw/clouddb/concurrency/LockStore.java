package de.designflaw.clouddb.concurrency;

public interface LockStore<K extends Comparable<K>> {
	public boolean tryLockRead(K key);

	public boolean tryLockWrite(K key);

	public void lockRead(K key);

	public void lockWrite(K key);

	public void freeLockRead(K key);

	public void freeLockWrite(K key);
}
