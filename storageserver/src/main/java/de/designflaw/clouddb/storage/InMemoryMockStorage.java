package de.designflaw.clouddb.storage;

import java.util.HashMap;
import java.util.Map;

public class InMemoryMockStorage<K extends Comparable<K>, V> implements
		Storage<K, V> {
	public Map<K, V> storage;

	public InMemoryMockStorage() {
		storage = new HashMap<K, V>();
	}

	@Override
	public boolean put(K key, V value) {
		boolean result = storage.containsKey(key);
		storage.put(key, value);
		return result;
	}

	@Override
	public Optional<V> get(K key) {
		if(storage.containsKey(key))
			return Optional.some(storage.get(key));
		return Optional.none();
	}

	@Override
	public boolean remove(K key) {
		if(!storage.containsKey(key))
			return false;
		storage.remove(key);
		return true;
	}

}
