package de.designflaw.clouddb.storage;

/**
 * The simplest possible storage interface.
 * 
 * @author flo
 *
 * @param <K>
 *            The key type
 * @param <V>
 *            The value type
 */
public interface Storage<K extends Comparable<K>, V> {

	/**
	 * Insert a new object
	 * 
	 * @param key
	 * @param value
	 * @return false for insertion, true for update
	 */
	public boolean put(K key, V value) throws StorageException;

	/**
	 * Retrieve an object
	 * 
	 * @param key
	 * @return
	 */
	public Optional<V> get(K key) throws StorageException;

	/**
	 * Remove an object
	 * 
	 * @param key
	 * @return false if it does not exist
	 */
	public boolean remove(K key) throws StorageException;
}
