package de.designflaw.clouddb.storage.caching;

import de.designflaw.clouddb.storage.Optional;


public interface Cache<K extends Comparable<K>,V> {

	/**
	 * Insert a new object
	 * 
	 * @param key
	 * @param value
	 */
	public void put(K key, V value);

	/**
	 * Retrieve an object
	 * 
	 * @param key
	 * @return
	 */
	public Optional<V> get(K key);

	/**
	 * Remove an object
	 * 
	 * @param key
	 */
	public void remove(K key);
}
