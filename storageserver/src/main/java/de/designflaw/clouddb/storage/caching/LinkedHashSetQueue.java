package de.designflaw.clouddb.storage.caching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;

/**
 * A data structure that has queue and set semantics at the same time. Elements
 * are ordered according to their insertion order. Additionally efficient
 * contains-queries are possible.
 * 
 * @author flo
 *
 * @param <T>
 */
public class LinkedHashSetQueue<T> implements Set<T>, Queue<T> {
	HashMap<T, Entry> set;

	Entry first, last;

	class Entry {
		T obj;
		Entry next, prev;

		Entry(T obj, Entry prev, Entry next) {
			this.obj = obj;
			this.prev = prev;
			this.next = next;
		}

		public boolean hasNext() {
			return next != null && (next.present() || next.hasNext());
		}

		public boolean present() {
			return set.containsKey(obj);
		}
	}

	public LinkedHashSetQueue() {
		set = new HashMap<T, Entry>();
		first = null;
		last = null;
	}

	@Override
	public int size() {
		return set.size();
	}

	@Override
	public boolean isEmpty() {
		return set.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return set.containsKey(o);
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Entry cur = first;

			@Override
			public boolean hasNext() {
				return cur != null;
			}

			@Override
			public T next() {
				T obj = cur.obj;
				cur = cur.next;
				return obj;
			}
		};
	}

	@Override
	public Object[] toArray() {
		ArrayList<T> arr = new ArrayList<T>(set.size());
		for (T elem : this) {
			arr.add(elem);
		}
		return arr.toArray();
	}

	@Override
	public <S> S[] toArray(S[] a) {
		ArrayList<T> arr = new ArrayList<T>(set.size());
		for (T elem : this) {
			arr.add(elem);
		}
		return arr.toArray(a);
	}

	@Override
	public boolean add(T e) {
		if (set.containsKey(e))
			return false;
		Entry entry = new Entry(e, last, null);
		if (last != null)
			last.next = entry;
		else
			first = entry;
		last = entry;
		set.put(e, entry);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		if (!set.containsKey(o))
			return false;
		Entry entry = set.remove(o);
		if (entry.next != null)
			entry.next.prev = entry.prev;
		else
			last = entry.prev;
		if (entry.prev != null)
			entry.prev.next = entry.next;
		else
			first = entry.next;
		return true;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object o : c) {
			if (!set.containsKey(o))
				return false;
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		for (T o : c) {
			if (!add(o))
				return false;
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		for (Object o : c) {
			if (!remove(o))
				return false;
		}
		return true;
	}

	@Override
	public void clear() {
		set.clear();
		first = null;
		last = null;
	}

	@Override
	public boolean offer(T e) {
		return add(e);
	}

	@Override
	public T remove() {
		if (first == null)
			throw new IllegalArgumentException("Empty queue");
		T obj = first.obj;
		remove(first.obj);
		return obj;
	}

	@Override
	public T poll() {
		T obj = first == null ? null : first.obj;
		if (obj != null)
			remove(first.obj);
		return obj;
	}

	@Override
	public T element() {
		if (first == null)
			throw new IllegalArgumentException("Empty queue");
		return first.obj;
	}

	@Override
	public T peek() {
		return first == null ? null : first.obj;
	}

}
