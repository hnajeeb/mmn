package de.designflaw.clouddb.storage;

public class StorageException extends Exception {
	private static final long serialVersionUID = 1L;
	Exception wrapped;
	String message;
	
	public StorageException(String message, Exception wrapped) {
		super(message);
		this.wrapped = wrapped;
	}
	
	public StorageException(String message) {
		super(message);
	}

	public StorageException(Exception wrapped) {
		super(wrapped.getMessage());
		this.wrapped = wrapped;
	}
}
