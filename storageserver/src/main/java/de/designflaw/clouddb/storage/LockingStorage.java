package de.designflaw.clouddb.storage;

import java.util.HashSet;
import java.util.Set;

import de.designflaw.clouddb.concurrency.KeyedLockStore;

/**
 * Adds locking (with an in-memory lock store) to a storage.
 * 
 * @author flo
 *
 * @param <K>
 * @param <V>
 */
public class LockingStorage<K extends Comparable<K>, V> implements
		Storage<K, V> {
	KeyedLockStore<K> locks;
	Storage<K, V> storage;
	Set<K> owning;

	public LockingStorage(Storage<K, V> storage, KeyedLockStore<K> locks) {
		this.locks = locks;
		this.storage = storage;
		this.owning = new HashSet<K>();
	}

	@Override
	public boolean put(K key, V value) throws StorageException {
		if (owning.contains(key))
			return storage.put(key, value);
		locks.lockWrite(key);
		boolean result = storage.put(key, value);
		locks.freeLockWrite(key);
		return result;
	}

	@Override
	public Optional<V> get(K key) throws StorageException {
		if (owning.contains(key))
			return storage.get(key);
		locks.lockRead(key);
		Optional<V> result = storage.get(key);
		locks.freeLockRead(key);
		return result;
	}

	@Override
	public boolean remove(K key) throws StorageException {
		if (owning.contains(key)) {
			return storage.remove(key);
		}
		locks.lockWrite(key);
		boolean result = storage.remove(key);
		locks.freeLockWrite(key);
		return result;
	}

	/**
	 * Explicitly lock object
	 * 
	 * @param key
	 */
	public void lock(K key) {
		locks.lockWrite(key);
		owning.add(key);
	}

	/**
	 * Unlock explicitly locket object
	 * 
	 * @param key
	 */
	public void unlock(K key) {
		locks.freeLockWrite(key);
		owning.remove(key);
	}
}
