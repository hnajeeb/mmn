package de.designflaw.clouddb.storage.caching;

import java.util.concurrent.ConcurrentLinkedQueue;

import de.designflaw.clouddb.storage.Optional;

class ConsistentCacheDecorator<K extends Comparable<K>, V> implements
		Cache<K, V> {
	Cache<K, V> store;
	ConcurrentLinkedQueue<K> invalidationQueue;

	ConsistentCacheRegistry<K,V> registry;

	ConsistentCacheDecorator(Cache<K, V> store,
			ConsistentCacheRegistry<K,V> registry) {
		this.store = store;
		this.invalidationQueue = new ConcurrentLinkedQueue<K>();
		this.registry = registry;
	}

	@Override
	public void put(K key, V value) {
		invalidate();
		store.put(key, value);
	}

	@Override
	public Optional<V> get(K key) {
		invalidate();
		return store.get(key);
	}

	@Override
	public void remove(K key) {
		invalidate();
		store.remove(key);
	}

	public void invalidate(K key) {
		invalidationQueue.add(key);
	}

	private void invalidate() {
		K k;
		while ((k = invalidationQueue.poll()) != null) {
			store.remove(k);
		}
	}

}
