package de.designflaw.clouddb.storage;

import de.designflaw.clouddb.storage.caching.Cache;
import de.designflaw.clouddb.storage.caching.ConsistentCacheRegistry;

/**
 * Adds caching to a storage
 * 
 * @author flo
 *
 * @param <K>
 * @param <V>
 */
public class ConsistentlyCachedStorage<K extends Comparable<K>, V> implements
		Storage<K, V> {
	Cache<K, V> cache;
	Storage<K, V> back;
	ConsistentCacheRegistry<K, V> registry;

	public ConsistentlyCachedStorage(Cache<K, V> cache, Storage<K, V> back,
			ConsistentCacheRegistry<K, V> registry) {
		this.registry = registry;
		this.cache = registry.addCoherency(cache);
		this.back = back;
	}

	@Override
	public boolean put(K key, V value) throws StorageException {
		registry.invalidate(key);
		return back.put(key, value);
	}

	@Override
	public Optional<V> get(K key) throws StorageException {
		Optional<V> fromCache = cache.get(key);
		if (fromCache.present())
			return fromCache;
		Optional<V> fromBack = back.get(key);
		if (fromBack.present())
			cache.put(key, fromBack.get());
		return fromBack;
	}

	@Override
	public boolean remove(K key) throws StorageException {
		registry.invalidate(key);
		return back.remove(key);
	}

}
