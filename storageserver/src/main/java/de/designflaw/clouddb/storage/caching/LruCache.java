package de.designflaw.clouddb.storage.caching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.designflaw.clouddb.storage.Optional;

/**
 * Cache with Least-Recently-Used eviction policy.
 * 
 * @author flo
 *
 */
public class LruCache<K extends Comparable<K>, V> implements Cache<K, V> {

	// Array that holds the values
	ArrayList<V> items;

	// Maps a key to a position in the value array
	Map<K, Integer> positions;

	// Queue to determine the oldest
	LinkedHashSetQueue<K> evictQueue;

	// Maximal capacity
	final int capacity;

	// Index in the array for the next insertion if cache not completely filled
	int pointer;

	class Freq implements Comparable<Freq> {
		int update;
		K key;

		@Override
		public int compareTo(LruCache<K, V>.Freq o) {
			return update - o.update;
		}
	}

	public LruCache(int capacity) {
		items = new ArrayList<V>(capacity);
		positions = new HashMap<K, Integer>(capacity);
		evictQueue = new LinkedHashSetQueue<K>();
		this.capacity = capacity;
		this.pointer = 0;
		for(int i = 0; i<capacity;i++)
			items.add(null);
	}

	@Override
	public void put(K key, V value) {
		if (positions.size() < capacity) {
			// direct insertion
			items.set(pointer, value);
			positions.put(key, pointer);
			evictQueue.add(key);
			pointer++;
		} else {
			// evict one;
			K evict = evictQueue.remove();
			int position = positions.remove(evict);
			items.set(position, value);
			positions.put(key, position);
			evictQueue.add(key);
		}
	}

	@Override
	public Optional<V> get(K key) {
		if (!positions.containsKey(key))
			return Optional.none();
		evictQueue.remove(key);
		evictQueue.offer(key);
		return Optional.of(items.get(positions.get(key)));
	}

	@Override
	public void remove(K key) {
		if (!positions.containsKey(key))
			return;
		pointer = positions.get(key);
		positions.remove(key);
		evictQueue.remove(key);
	}

}
