package de.designflaw.clouddb.storage.caching;

import java.util.concurrent.ConcurrentLinkedQueue;

import de.designflaw.clouddb.storage.Optional;

/**
 * Decorates a Cache with invalidation support (caches are connected to each
 * other via a ConsistentCacheRegistry that makes sure cached elements are
 * removed when they change).
 * 
 * @author flo
 *
 * @param <K>
 * @param <V>
 */
public abstract class ConsistentCache<K extends Comparable<K>, V> implements
		Cache<K, V> {
	ConcurrentLinkedQueue<K> invalidationQueue;

	ConsistentCacheRegistry<K, V> registry;

	ConsistentCache() {
		this.invalidationQueue = new ConcurrentLinkedQueue<K>();
	}

	public void setRegistry(ConsistentCacheRegistry<K, V> registry) {
		this.registry = registry;
	}

	@Override
	public abstract void put(K key, V value);

	@Override
	public abstract Optional<V> get(K key);

	@Override
	public abstract void remove(K key);

	public void invalidate(K key) {
		invalidationQueue.add(key);
	}

	protected void invalidate() {
		K k;
		while ((k = invalidationQueue.poll()) != null) {
			this.remove(k);
		}
	}

}
