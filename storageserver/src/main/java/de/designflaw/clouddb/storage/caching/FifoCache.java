package de.designflaw.clouddb.storage.caching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.designflaw.clouddb.storage.Optional;

public class FifoCache<K extends Comparable<K>, V> implements Cache<K, V> {

	// Array that holds the values
	ArrayList<V> items;

	// Maps a key to a position in the value array
	Map<K, Integer> positions;

	// Reverse mapping of positions: Assigns an index in the value array to a
	// key
	Map<Integer, K> keys;

	// Capacity of the cache
	final int capacity;

	// Index in the value array for the next insertion
	int pointer;

	public FifoCache(int capacity) {
		this.capacity = capacity;
		this.items = new ArrayList<V>(capacity);
		this.pointer = 0;
		this.positions = new HashMap<K, Integer>(capacity);
		this.keys = new HashMap<Integer, K>(capacity);
		for (int i = 0; i < capacity; i++)
			items.add(null);
	}

	@Override
	public void put(K key, V value) {
		items.set(pointer, value);
		positions.remove(keys.get(pointer));
		positions.put(key, pointer);
		keys.put(pointer, key);

		pointer = (1 + pointer) % capacity;
	}

	@Override
	public Optional<V> get(K key) {
		if (!positions.containsKey(key))
			return Optional.none();
		return Optional.of(items.get(positions.get(key)));
	}

	@Override
	public void remove(K key) {
		if (!positions.containsKey(key))
			return;
		positions.remove(key);
	}

}
