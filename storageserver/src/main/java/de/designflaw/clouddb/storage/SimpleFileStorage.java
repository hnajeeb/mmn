package de.designflaw.clouddb.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class SimpleFileStorage implements Storage<String, String> {
	File dir;

	private static final Logger logger = LogManager.getRootLogger();

	public static final String suffix = ".data";

	public SimpleFileStorage(String directory) {
		dir = new File(directory);
	}

	@Override
	public boolean put(String key, String value) throws StorageException {
		key = keyTransform(key);
		try {
			boolean status = objectFile(key).exists();
			if (!status)
				objectFile(key).createNewFile();
			if (!objectFile(key).canWrite()) {
				logger.error("Cannot write to "
						+ objectFile(key).getAbsolutePath());
				throw new StorageException("Cannot write to "
						+ objectFile(key).getAbsolutePath());
			}
			OutputStream outStream = new FileOutputStream(objectFile(key),
					false);
			Writer outWriter = new PrintWriter(outStream);
			outWriter.write(value);
			outWriter.flush();
			outWriter.close();
			return status;
		} catch (IOException e) {
			logger.error(e);
			throw new StorageException(e);
		}
	}

	@Override
	public Optional<String> get(String key) throws StorageException {
		key = keyTransform(key);
		try {
			if (!objectFile(key).exists())
				return Optional.none();
			if (!objectFile(key).canRead()) {
				logger.error("Cannot read from "
						+ objectFile(key).getAbsolutePath());
				throw new StorageException("Cannot read from "
						+ objectFile(key).getAbsolutePath());
			}
			return Optional.some(getContent(objectFile(key)));
		} catch (IOException e) {
			logger.error(e);
			throw new StorageException(e);
		}
	}

	@Override
	public boolean remove(String key) throws StorageException {
		key = keyTransform(key);
		try {
			if (!objectFile(key).exists()) {
				logger.warn("Object " + key + " to remove does not exist.");
				return false;
			}
			if (!objectFile(key).delete()) {
				logger.warn("Object " + key + " could not be deleted.");
				throw new StorageException("Object " + key
						+ " could not be deleted.");
			}
			return true;
		} catch (IOException e) {
			logger.error(e);
			throw new StorageException(e);
		}
	}

	private File objectFile(String key) throws IOException {
		if (!dir.exists())
			dir.mkdirs();
		if (!dir.isDirectory()) {
			logger.fatal(dir.getAbsolutePath() + " is no directory");
			throw new FileNotFoundException();
		}
		File file = new File(dir.getCanonicalPath() + File.separator + key
				+ suffix);
		return file;
	}

	private String getContent(File file) throws IOException {
		InputStream inStream = new FileInputStream(file);
		InputStreamReader inReader = new InputStreamReader(inStream);
		StringBuffer buf = new StringBuffer();
		int available;
		while ((available = inStream.available()) > 0) {
			char[] cbuf = new char[available];
			inReader.read(cbuf);
			buf.append(cbuf);
		}
		inReader.close();
		return buf.toString();
	}

	/**
	 * Transforms the key (e.g. hash function or Base64 encoding). Extend
	 * SimpleFileStorage to change behavior.
	 * 
	 * @param key
	 * @return
	 */
	public String keyTransform(String key) {
		try {
			return URLEncoder.encode(key, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.fatal(e);
		}
		return null;
	}
}
