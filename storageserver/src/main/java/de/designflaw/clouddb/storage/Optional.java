package de.designflaw.clouddb.storage;

public class Optional<T> {
	private T obj;

	private Optional(T obj) {
		this.obj = obj;
	}

	public static <T> Optional<T> some(T obj) {
		if (obj == null)
			throw new IllegalArgumentException("Null given.");
		else
			return new Optional<T>(obj);
	}

	public static <T> Optional<T> none() {
		return new Optional<T>(null);
	}

	public static <T> Optional<T> of(T obj) {
		return new Optional<T>(obj);
	}

	public boolean present() {
		return obj != null;
	}

	public T get() {
		if (obj == null)
			throw new IllegalArgumentException("No element present.");
		else
			return obj;
	}

	public T getOrNull() {
		return obj;
	}

	public <S extends T> T or(S other) {
		if (obj == null)
			return other;
		else
			return obj;
	}

	public <S extends T> Optional<? extends T> or(Optional<S> other) {
		if (obj == null)
			return other;
		else
			return Optional.of(obj);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object o) {
		if (o == null && obj == null)
			return true;
		if (obj == null)
			return false;
		if (!(o instanceof Optional))
			return false;
		return obj.equals(((Optional) o).obj);
	}

	@Override
	public String toString() {
		return obj == null ? "None" : "Some(" + obj.toString() + ")";
	}
}
