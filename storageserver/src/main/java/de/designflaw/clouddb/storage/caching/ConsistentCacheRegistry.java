package de.designflaw.clouddb.storage.caching;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Keeps links of all caches that over a persistent storage to remove elements
 * from all when an entry in the persistent background storage changes.
 * 
 * @author flo
 *
 * @param <K>
 * @param <V>
 */
public class ConsistentCacheRegistry<K extends Comparable<K>, V> {
	ConcurrentLinkedQueue<ConsistentCacheDecorator<K, V>> caches;

	public ConsistentCacheRegistry() {
		this.caches = new ConcurrentLinkedQueue<ConsistentCacheDecorator<K, V>>();
	}

	public void invalidate(K key) {
		for (ConsistentCacheDecorator<K, V> cache : caches)
			cache.invalidate(key);
	}

	public Cache<K, V> addCoherency(Cache<K, V> cache) {
		ConsistentCacheDecorator<K, V> cc = new ConsistentCacheDecorator<K, V>(
				cache, this);
		caches.add(cc);
		return cc;
	}
}
