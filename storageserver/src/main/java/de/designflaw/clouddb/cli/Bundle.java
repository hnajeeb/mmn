package de.designflaw.clouddb.cli;

public interface Bundle<T> {
	public T getState();

	public Iterable<CommandLineInterface<T>> getHandlers();
}
