package de.designflaw.clouddb.cli;

import java.io.InputStream;
import java.io.PrintStream;

public interface StateCallback<T> {
	public void setPromt(String promt);

	public void setExit(boolean exit);

	public T getState();

	public PrintStream getOut();

	public InputStream getIn();
}
