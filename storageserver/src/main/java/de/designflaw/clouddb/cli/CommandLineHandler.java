package de.designflaw.clouddb.cli;

/**
 * Describes a handler for a command line command.
 * 
 * @author flo
 *
 * @param <T>
 */
public interface CommandLineHandler<T> {

	/**
	 * Is called by the command line after the user submitted a command. If the
	 * command can be processed by the handler, it should do so and return true,
	 * otherwise just return false.
	 * 
	 * @param command
	 *            The command submitted by the user
	 * @param ifaceRef
	 *            A reference to the command line interface. The handler can use
	 *            its input and output stream and alter the accompanying state
	 *            object.
	 * @return True after the command has been (successfully/unsuccessfully)
	 *         processed, false otherwise.
	 */
	public boolean tryExec(String command, StateCallback<T> ifaceRef);
}
