package de.designflaw.clouddb.cli;

import java.util.regex.Pattern;

public class ExitHandler<T> implements CommandLineHandler<T> {
	static final Pattern regex = Pattern.compile("^exit\\s*$");

	public boolean tryExec(String command, StateCallback<T> ifaceRef) {
		if (regex.matcher(command).matches()) {
			ifaceRef.setExit(true);
			return true;
		}
		return false;
	}
}
