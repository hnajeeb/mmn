package de.designflaw.clouddb.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CommandLineInterface<T> implements CLIExecutable, StateCallback<T> {
	protected String promt;
	protected Iterable<CommandLineHandler<T>> handlers;
	protected boolean exit = false;

	protected T cliState;

	private final PrintStream out;
	private final InputStream in;

	private static final Logger logger = LogManager.getRootLogger();

	/**
	 * 
	 * @param promt
	 *            Text that is shown to the user to indicate waiting for a new
	 *            command
	 * @param handlers
	 *            Handlers for command line commands
	 * @param state
	 *            Holds the state of the command line application
	 * @param out
	 *            Output for command line message
	 * @param in
	 *            Input for command line commands
	 */
	public CommandLineInterface(String promt,
			Iterable<CommandLineHandler<T>> handlers, T state, PrintStream out,
			InputStream in) {
		this.promt = promt;
		this.handlers = handlers;
		this.cliState = state;
		this.out = out;
		this.in = in;
	}

	public CommandLineInterface(String promt,
			Iterable<CommandLineHandler<T>> handlers, T state) {
		this(promt, handlers, state, System.out, System.in);
	}

	public void setPromt(String promt) {
		this.promt = promt;
	}

	public void setExit(boolean exit) {
		this.exit = exit;
	}

	public T getState() {
		return cliState;
	}

	public PrintStream getOut() {
		return out;
	}

	public InputStream getIn() {
		return in;
	}

	private BufferedReader inputReader() {
		return new BufferedReader(new InputStreamReader(in));
	}

	@Override
	public void exec() {
		try {
			while (!exit) {
				out.print(promt);
				BufferedReader in = inputReader();
				String cmd = in.readLine();
				boolean executed = false;
				for (CommandLineHandler<T> handler : handlers)
					if (handler.tryExec(cmd, this)) {
						logger.info("Command \"" + cmd + "\" invoked handler "
								+ handler.getClass().getCanonicalName());
						executed = true;
						break;
					}
				if (!executed) {
					out.println("No handler found.");
					logger.warn("Command \"" + cmd + "\" is not recognised");
				}
			}
			logger.log(Level.INFO, "Exiting from command line");
			in.close();
		} catch (IOException e) {
			logger.log(Level.FATAL, e);
		}
	}
}
