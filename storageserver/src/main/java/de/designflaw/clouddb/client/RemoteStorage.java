package de.designflaw.clouddb.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.storage.Optional;
import de.designflaw.clouddb.storage.Storage;
import de.designflaw.clouddb.storage.StorageException;

public class RemoteStorage implements Storage<String, String> {
	BufferedReader reader;
	PrintWriter writer;
	Socket sock;

	private static final Logger logger = LogManager.getRootLogger();

	private static Pattern putSuccess = Pattern.compile("^PUT_SUCCESS (.+)$");
	private static Pattern putUpdate = Pattern.compile("^PUT_UPDATE (.+)$");
	private static Pattern putError = Pattern.compile("^PUT_ERROR (.+)$");

	private static Pattern getSuccess = Pattern
			.compile("^GET_SUCCESS (.+) (.*)$");
	private static Pattern getUnknown = Pattern
			.compile("^GET_ERROR .* does not exist");
	private static Pattern getError = Pattern.compile("^GET_ERROR (.+)$");

	private static Pattern deleteSuccess = Pattern.compile("^DELETE_SUCCESS");
	private static Pattern deleteUnknown = Pattern
			.compile("^DELETE_ERROR Does not exist");
	private static Pattern deleteError = Pattern.compile("^GET_ERROR (.+)$");

	public static RemoteStorage open(String host, int port)
			throws UnknownHostException, IOException {
		RemoteStorage storage = new RemoteStorage();
		storage.sock = new Socket(host, port);
		storage.reader = new BufferedReader(new InputStreamReader(
				storage.sock.getInputStream()));
		storage.writer = new PrintWriter(storage.sock.getOutputStream());
		return storage;
	}

	public void close() throws IOException {
		writer.println("QUIT");
		writer.flush();
		writer.close();
		reader.close();
		sock.close();
	}

	@Override
	public boolean put(String key, String value) throws StorageException {
		try {
			String msg = "PUT_ENC " + URLEncoder.encode(key, "UTF-8") + " "
					+ URLEncoder.encode(value, "UTF-8");
			logger.debug("Sending " + msg);
			writer.println(msg);
			writer.flush();
			String response = reader.readLine();
			logger.debug("Received " + response);
			if (putSuccess.matcher(response).matches()) {
				return false;
			}
			if (putUpdate.matcher(response).matches()) {
				return true;
			}
			Matcher error = putError.matcher(response);
			if (error.matches()) {
				String message = error.group(1);
				throw new StorageException(message);
			}
			throw new StorageException("Unparsable response");
		} catch (Exception e) {
			logger.error(e);
			throw new StorageException(e);
		}
	}

	@Override
	public Optional<String> get(String key) throws StorageException {
		try {
			String msg = "GET_ENC " + URLEncoder.encode(key, "UTF-8");
			logger.debug("Sending " + msg);
			writer.println(msg);
			writer.flush();
			String response = reader.readLine();
			logger.debug("Received " + response);
			Matcher success = getSuccess.matcher(response);
			if (success.matches()) {
				String result = URLDecoder.decode(success.group(2), "UTF-8");
				return Optional.some(result);
			}
			if (getUnknown.matcher(response).matches()) {
				return Optional.none();
			}
			Matcher error = getError.matcher(response);
			if (error.matches()) {
				String message = error.group(1);
				throw new StorageException(message);
			}
			throw new StorageException("Unparsable response");
		} catch (Exception e) {
			logger.error(e);
			throw new StorageException(e);
		}
	}

	@Override
	public boolean remove(String key) throws StorageException {
		try {
			String msg = "DELETE_ENC " + URLEncoder.encode(key, "UTF-8");
			logger.debug("Sending " + msg);
			writer.println(msg);
			writer.flush();
			String response = reader.readLine();
			logger.debug("Received " + response);
			if (deleteSuccess.matcher(response).matches()) {
				return true;
			}
			if (deleteUnknown.matcher(response).matches()) {
				return false;
			}
			Matcher error = deleteError.matcher(response);
			if (error.matches()) {
				String message = error.group(1);
				throw new StorageException(message);
			}
			throw new StorageException("Unparsable response");
		} catch (Exception e) {
			logger.error(e);
			throw new StorageException(e);
		}
	}

}
