package de.designflaw.clouddb.server;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CLIExecutable;
import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.CommandLineInterface;
import de.designflaw.clouddb.cli.StateCallback;
import de.designflaw.clouddb.storage.Optional;
import de.designflaw.clouddb.storage.Storage;
import de.designflaw.clouddb.storage.StorageException;

public class Interface implements CLIExecutable {
	Socket sock;
	CommandLineInterface<SessionState> cli;

	private static final Logger logger = LogManager.getRootLogger();

	public static Interface init(Socket socket, Storage<String, String> storage) {
		Interface iface = new Interface();
		iface.sock = socket;
		List<CommandLineHandler<SessionState>> handlers = new ArrayList<CommandLineHandler<SessionState>>();
		handlers.add(new PutCommand());
		handlers.add(new PutEncCommand());
		handlers.add(new GetCommand());
		handlers.add(new GetEncCommand());
		handlers.add(new DeleteCommand());
		handlers.add(new DeleteEncCommand());
		handlers.add(new QuitCommand());
		handlers.add(new WildcardCommand());
		try {
			iface.cli = new CommandLineInterface<SessionState>("", handlers,
					new SessionState(storage), new PrintStream(
							socket.getOutputStream()), socket.getInputStream());
		} catch (IOException e) {
			logger.fatal(e);
		}
		return iface;
	}

	@Override
	public void exec() {
		cli.exec();
	}

	static class PutCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern
				.compile("^PUT\\s+(.+?)\\s+(.+)$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			SessionState st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key = match.group(1);
			String value = match.group(2);
			Boolean status = null;
			try {
				status = st.storage.put(key, value);
			} catch (StorageException e) {
				logger.warn(e);
				ifaceRef.getOut().println("PUT_ERROR " + e.getMessage());
				return true;
			}
			ifaceRef.getOut().println(
					(status ? "PUT_UPDATE " : "PUT_SUCCESS ") + key);
			return true;
		}
	}

	static class PutEncCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern
				.compile("^PUT_ENC\\s+(.+)\\s+(.+)\\s*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			SessionState st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key, value = null;
			try {
				key = URLDecoder.decode(match.group(1), "UTF-8");
				value = URLDecoder.decode(match.group(2), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error(e);
				ifaceRef.getOut().println("PUT_ERROR " + e.getMessage());
				return true;
			}
			Boolean status = null;
			try {
				status = st.storage.put(key, value);
			} catch (StorageException e) {
				logger.warn(e);
				ifaceRef.getOut().println("PUT_ERROR " + e.getMessage());
				return true;
			}
			ifaceRef.getOut().println(
					(status ? "PUT_UPDATE " : "PUT_SUCCESS ") + key);
			return true;
		}
	}

	static class GetCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern.compile("^GET\\s+(.+)\\s*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			SessionState st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key = match.group(1);
			Optional<String> res = null;
			try {
				res = st.storage.get(key);
			} catch (StorageException e) {
				logger.warn(e);
				ifaceRef.getOut().println("GET_ERROR " + e.getMessage());
				return true;
			}
			if (res.present()) {
				ifaceRef.getOut().println(
						"GET_SUCCESS " + key + " " + res.get());
			} else {
				ifaceRef.getOut().println(
						"GET_ERROR " + key + " does not exist");
			}
			return true;
		}
	}

	static class GetEncCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern.compile("^GET_ENC\\s+(.+)\\s*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			SessionState st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key;
			try {
				key = URLDecoder.decode(match.group(1), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error(e);
				ifaceRef.getOut().println("GET_ERROR " + e.getMessage());
				return true;
			}
			Optional<String> res = null;
			try {
				res = st.storage.get(key);
			} catch (StorageException e) {
				logger.warn(e);
				ifaceRef.getOut().println("GET_ERROR " + e.getMessage());
				return true;
			}
			if (res.present()) {
				try {
					ifaceRef.getOut().println(
							"GET_SUCCESS " + URLEncoder.encode(key, "UTF-8")
									+ " "
									+ URLEncoder.encode(res.get(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					logger.error(e);
					ifaceRef.getOut().println("GET_ERROR " + e.getMessage());
					return true;
				}
			} else {
				ifaceRef.getOut().println(
						"GET_ERROR " + key + " does not exist");
			}
			return true;
		}
	}

	static class DeleteCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern.compile("^DELETE\\s+(.+)\\s*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			SessionState st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key = match.group(1);
			Boolean status = null;
			try {
				status = st.storage.remove(key);
			} catch (StorageException e) {
				logger.warn(e);
				ifaceRef.getOut().println("DELETE_ERROR " + e.getMessage());
				return true;
			}
			if (status)
				ifaceRef.getOut().println("DELETE_SUCCESS");
			else
				ifaceRef.getOut().println("DELETE_ERROR Does not exist");
			return true;
		}
	}

	static class DeleteEncCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern
				.compile("^DELETE_ENC\\s+(.+)\\s*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			SessionState st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key;
			try {
				key = URLDecoder.decode(match.group(1), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error(e);
				ifaceRef.getOut().println("GET_ERROR " + e.getMessage());
				return true;
			}
			Boolean status = null;
			try {
				status = st.storage.remove(key);
			} catch (StorageException e) {
				logger.warn(e);
				ifaceRef.getOut().println("DELETE_ERROR " + e.getMessage());
			}
			if (status)
				ifaceRef.getOut().println("DELETE_SUCCESS");
			else
				ifaceRef.getOut().println("DELETE_ERROR Does not exist");
			return true;
		}
	}

	static class QuitCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern.compile("^QUIT\\s*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			ifaceRef.getOut().println("QUIT_SUCCESS Goodbye!");
			ifaceRef.getOut().close();
			ifaceRef.setExit(true);
			return true;
		}
	}

	static class WildcardCommand implements CommandLineHandler<SessionState> {
		private static Pattern regex = Pattern.compile("^.*$");

		@Override
		public boolean tryExec(String command,
				StateCallback<SessionState> ifaceRef) {
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			logger.warn("Server received unknown command: " + command);
			ifaceRef.getOut().println("ERROR Command not interpretable");
			return true;
		}
	}
}
