package de.designflaw.clouddb.server;

import java.io.IOException;
import java.net.Socket;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.storage.Storage;

/**
 * A worker runs in one thread and handles one client session per time. When a
 * session is closed it acquires new work from the Schduler.
 * 
 * @author flo
 *
 */
public class Worker implements Runnable {
	Scheduler scheduler;
	Storage<String, String> storage;

	private static final Logger logger = LogManager.getRootLogger();

	static Worker init(StorageProvider storageProvider, Scheduler scheduler) {
		Worker w = new Worker();
		w.storage = storageProvider.get();
		w.scheduler = scheduler;
		return w;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			Socket sock = scheduler.assign(this);
			if (sock == null)
				continue;
			Interface.init(sock, storage).exec();
			try {
				sock.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		logger.info("Worker suspended");
	}

}
