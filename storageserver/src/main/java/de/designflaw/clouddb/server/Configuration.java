package de.designflaw.clouddb.server;

import java.util.Properties;
import java.util.Random;

public class Configuration {
	public enum CacheStrategy {
		FIFO, LRU, LFU
	}

	public String directory = "/home/flo/clouddb/store";
	public int workers = 8;
	public int cacheSize = 100;
	public int port = 50000;
	public CacheStrategy cacheStrategy = CacheStrategy.FIFO;

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public void setWorkers(int workers) {
		this.workers = workers;
	}

	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setCacheStrategy(CacheStrategy cacheStrategy) {
		this.cacheStrategy = cacheStrategy;
	}

	public int getWorkers() {
		return workers;
	}

	public int getCacheSize() {
		return cacheSize;
	}

	public int getPort() {
		return port;
	}

	public CacheStrategy getCacheStrategy() {
		return cacheStrategy;
	}

	private static Configuration inst;

	public static Configuration instance() {
		if (inst == null)
			inst = new Configuration();
		return inst;
	}

	public static Configuration testConfig() {
		Configuration c = new Configuration();
		c.directory = ".clouddb/teststorage-" + new Random().nextInt(1000);
		return c;
	}

	public static Configuration fromProperties(Properties props) {
		Configuration c = new Configuration();
		c.directory = props.getProperty("directory", c.directory);
		return c;
	}
}
