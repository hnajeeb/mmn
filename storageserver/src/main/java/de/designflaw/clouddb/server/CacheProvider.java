package de.designflaw.clouddb.server;

import de.designflaw.clouddb.storage.caching.Cache;
import de.designflaw.clouddb.storage.caching.FifoCache;
import de.designflaw.clouddb.storage.caching.LruCache;

public class CacheProvider {
	Configuration config;

	public CacheProvider(Configuration config) {
		this.config = config;
	}

	public Cache<String, String> get() {
		switch (config.cacheStrategy) {
		case FIFO:
			return new FifoCache<String, String>(config.getCacheSize());
		case LRU:
			return new LruCache<String, String>(config.getCacheSize());
		default:
			return new FifoCache<String, String>(config.getCacheSize());
		}
	}
}
