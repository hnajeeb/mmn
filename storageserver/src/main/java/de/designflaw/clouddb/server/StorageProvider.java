package de.designflaw.clouddb.server;

import de.designflaw.clouddb.concurrency.KeyedLockStore;
import de.designflaw.clouddb.storage.LockingStorage;
import de.designflaw.clouddb.storage.SimpleFileStorage;
import de.designflaw.clouddb.storage.Storage;
import de.designflaw.clouddb.storage.ConsistentlyCachedStorage;
import de.designflaw.clouddb.storage.caching.ConsistentCacheRegistry;

public class StorageProvider {
	Configuration config;
	KeyedLockStore<String> locks;
	ConsistentCacheRegistry<String, String> registry;
	CacheProvider cacheProvider;

	public StorageProvider(Configuration config) {
		this.config = config;
		this.locks = new KeyedLockStore<String>();
		this.registry = new ConsistentCacheRegistry<String, String>();
		this.cacheProvider = new CacheProvider(config);
	}

	public Storage<String, String> get() {
		return new LockingStorage<String, String>(
				new ConsistentlyCachedStorage<String, String>(
						cacheProvider.get(), new SimpleFileStorage(
								config.getDirectory()), registry), locks);
	}

}
