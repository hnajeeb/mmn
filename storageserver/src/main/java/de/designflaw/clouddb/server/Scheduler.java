package de.designflaw.clouddb.server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Scheduler {
	ConcurrentLinkedQueue<Socket> connections;
	StorageProvider storageProvider;
	List<Thread> workers;
	Set<Worker> freeWorkers;

	public static Scheduler init(StorageProvider storageProvider) {
		Scheduler scheduler = new Scheduler();
		scheduler.connections = new ConcurrentLinkedQueue<Socket>();
		scheduler.workers = new ArrayList<Thread>();
		scheduler.freeWorkers = new HashSet<Worker>();
		scheduler.storageProvider = storageProvider;
		return scheduler;
	}

	synchronized void offer(Socket sock) {
		connections.add(sock);
		if (freeWorkers.isEmpty())
			addWorker();
		notify();
	}

	synchronized Socket assign(Worker worker) {
		Socket sock = null;
		while ((sock = connections.poll()) == null) {
			freeWorkers.add(worker);
			try {
				wait();
			} catch (InterruptedException e) {
				return null;
			}
			freeWorkers.remove(worker);
		}
		return sock;
	}

	private void addWorker() {
		Thread worker = new Thread(Worker.init(storageProvider, this));
		workers.add(worker);
		worker.start();
	}
}
