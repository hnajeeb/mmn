package de.designflaw.clouddb.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CLIExecutable;
import de.designflaw.clouddb.concurrency.KeyedLockStore;
import de.designflaw.clouddb.storage.caching.ConsistentCacheRegistry;

/**
 * Base server runtime. Listens for incoming requests and puts them in the
 * schedulers work queue. To use, wrap it in a Thread or call exec directly
 * 
 * @author flo
 *
 */
public class Server implements CLIExecutable, Runnable {
	Scheduler scheduler;
	KeyedLockStore<String> locks;
	ConsistentCacheRegistry<String, String> registry;
	Configuration config;
	ServerSocket sock;

	private static final Logger logger = LogManager.getRootLogger();

	public static Server init(Configuration config) {
		Server s = new Server();
		s.config = config;
		s.scheduler = Scheduler.init(new StorageProvider(config));
		return s;
	}

	public ServerSocket getSocket() {
		return sock;
	}

	@Override
	public void run() {
		exec();
	}

	@Override
	public void exec() {
		try {
			sock = new ServerSocket(config.getPort());
			while (!Thread.interrupted() && !sock.isClosed()) {
				try {
					scheduler.offer(sock.accept());
				} catch (SocketException e) {
					logger.debug(e);
				}
			}
			if (!sock.isClosed())
				sock.close();
		} catch (IOException e) {
			logger.fatal(e);
		}
	}

	public static void main(String args[]) {
		Server.init(Configuration.testConfig()).exec();
	}

}
