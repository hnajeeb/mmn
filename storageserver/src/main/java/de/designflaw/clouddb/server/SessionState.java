package de.designflaw.clouddb.server;

import de.designflaw.clouddb.storage.Storage;

public class SessionState {
	public Storage<String, String> storage;

	public SessionState(Storage<String, String> storage) {
		this.storage = storage;
	}
}
