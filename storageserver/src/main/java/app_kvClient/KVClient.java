package app_kvClient;

import java.io.IOException;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.designflaw.clouddb.cli.CommandLineHandler;
import de.designflaw.clouddb.cli.CommandLineInterface;
import de.designflaw.clouddb.cli.ExitHandler;
import de.designflaw.clouddb.cli.StateCallback;
import de.designflaw.clouddb.echoclient.EchoClient;
import de.designflaw.clouddb.server.Configuration;
import de.designflaw.clouddb.server.Configuration.CacheStrategy;
import de.designflaw.clouddb.server.Server;

public class KVClient {

	CommandLineInterface<State> cli;

	private static final Logger logger = LogManager.getRootLogger();

	static class State {
		public Configuration config = Configuration.testConfig();
		public Server server = null;
		public Thread serverThread = null;
	}

	public static void exec() {
		// CommandLineInterface(String promt,
		// Iterable<CommandLineHandler<T>> handlers, T state, PrintStream out,
		// InputStream in)
		KVClient c = new KVClient();
		LinkedList<CommandLineHandler<State>> handlers = new LinkedList<CommandLineHandler<State>>();
		handlers.add(new SetCommand());
		handlers.add(new ServerStartCommand());
		handlers.add(new ServerStopCommand());
		handlers.add(new HelpCommand());
		handlers.add(new ClientCommand());
		handlers.add(new ExitHandler<State>());
		c.cli = new CommandLineInterface<State>("Storage > ", handlers,
				new State(), System.out, System.in);
		c.cli.exec();
	}

	public static void main(String[] args) {
		exec();
	}

	/**
	 * Change configuration parameters
	 * 
	 * @author flo
	 *
	 */
	static class SetCommand implements CommandLineHandler<State> {
		private static Pattern regex = Pattern
				.compile("^set\\s+(.+)\\s+(.+)\\s*$");

		@Override
		public boolean tryExec(String command, StateCallback<State> ifaceRef) {
			State st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			String key = match.group(1);
			String val = match.group(2);

			if (key.equals("directory"))
				st.config.setDirectory(val);
			if (key.equals("workers"))
				st.config.setWorkers(Integer.parseInt(val));
			if (key.equals("cacheSize"))
				st.config.setCacheSize(Integer.parseInt(val));
			if (key.equals("port"))
				st.config.setPort(Integer.parseInt(val));
			if (key.equals("cacheStrategy"))
				st.config.setCacheStrategy(CacheStrategy.valueOf(val));
			return true;
		}
	}

	/**
	 * Starts the storage server
	 * 
	 * @author flo
	 *
	 */
	static class ServerStartCommand implements CommandLineHandler<State> {
		private static Pattern regex = Pattern.compile("^server start\\s*$");

		@Override
		public boolean tryExec(String command, StateCallback<State> ifaceRef) {
			State st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			if (st.server != null) {
				ifaceRef.getOut().println("The server is already running");
				return true;
			}
			st.server = Server.init(st.config);
			st.serverThread = new Thread(st.server);
			st.serverThread.start();
			ifaceRef.getOut().println(
					"Server is listening on port " + st.config.port);
			ifaceRef.getOut().println(
					"Storage location is " + st.config.directory);
			ifaceRef.getOut().println("Use telnet for access");
			ifaceRef.getOut().println("Type \"server stop\" to stop");
			return true;
		}
	}

	static class ServerStopCommand implements CommandLineHandler<State> {
		private static Pattern regex = Pattern.compile("^server stop\\s*$");

		@Override
		public boolean tryExec(String command, StateCallback<State> ifaceRef) {
			State st = ifaceRef.getState();
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			if (st.server == null) {
				ifaceRef.getOut().println("The server is not running");
				return true;
			}
			try {
				st.server.getSocket().close();
			} catch (IOException e) {
				logger.equals(e);
			}
			st.serverThread.interrupt();
			st.server = null;
			ifaceRef.getOut().println("Storage Server stopped");
			return true;
		}
	}

	static class HelpCommand implements CommandLineHandler<State> {
		private static Pattern regex = Pattern.compile("^help\\s*$");
		private static String format = "%30s %50s\n";

		@Override
		public boolean tryExec(String command, StateCallback<State> ifaceRef) {
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			@SuppressWarnings("resource")
			Formatter out = new Formatter(ifaceRef.getOut());
			out.format(format, "help", "Print this information");
			out.format(format, "set <key> <value>", "Set configuration value");
			out.format(format, "server start", "Start the server");
			out.format(format, "server stop", "Stop the server");
			out.format(format, "echoclient", "Runs the echoclient");
			out.format(format, "exit", "Exit from command line");
			out.flush();
			return true;
		}
	}

	static class ClientCommand implements CommandLineHandler<State> {
		private static Pattern regex = Pattern.compile("^echoclient\\s*$");

		@Override
		public boolean tryExec(String command, StateCallback<State> ifaceRef) {
			Matcher match = regex.matcher(command);
			if (!match.matches())
				return false;
			new EchoClient(ifaceRef.getIn(), ifaceRef.getOut()).exec();
			return true;
		}
	}

}
