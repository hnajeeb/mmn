package de.designflaw.clouddb.storage.caching;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

public class CacheInvalidationTest {
	ConsistentCacheRegistry<Integer, String> registry;
	Cache<Integer, String> mock;

	@Before
	public void setup() {
		registry = new ConsistentCacheRegistry<Integer, String>();

	}

	@SuppressWarnings("unchecked")
	@Test
	public void invalidationTriggered() {
		mock = (Cache<Integer, String>) mock(Cache.class);
		Cache<Integer, String> mocked = registry.addCoherency(mock);

		registry.invalidate(1);
		mocked.get(2);

		verify(mock).remove(1);
		verify(mock).get(2);
	}
}
