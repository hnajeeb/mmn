package de.designflaw.clouddb.client;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.designflaw.clouddb.server.Configuration;
import de.designflaw.clouddb.server.Server;
import de.designflaw.clouddb.storage.StorageException;

public class RemoteStorageDataTest {
	Server server;
	Thread serverThread;

	@Before
	public void setUp() throws UnknownHostException, IOException,
			InterruptedException {
		server = Server.init(Configuration.testConfig());
		serverThread = new Thread(server);
		serverThread.start();
	}

	@After
	public void tearDown() throws IOException {
		server.getSocket().close();
	}

	@Test
	public void writeReadDelete() throws StorageException, UnknownHostException, IOException {
		RemoteStorage client = RemoteStorage.open("localhost",
				Configuration.testConfig().port);
		String key = "testKey-" + new Random().nextInt(1000);
		assertFalse(client.put(key, "testValue?\n"));
		assertEquals(client.get(key).getOrNull(), "testValue?\n");
		assertTrue(client.remove(key));
		assertFalse(client.get(key).present());
		client.close();
	}
}
