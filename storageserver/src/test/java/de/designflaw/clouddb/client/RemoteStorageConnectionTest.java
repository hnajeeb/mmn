package de.designflaw.clouddb.client;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.designflaw.clouddb.server.Configuration;
import de.designflaw.clouddb.server.Server;

public class RemoteStorageConnectionTest {
	Server server;
	Thread serverThread;

	@Before
	public void setUp() {
		server = Server.init(Configuration.testConfig());
		serverThread = new Thread(server);
		serverThread.start();
	}

	@After
	public void tearDown() throws IOException {
		server.getSocket().close();
	}

	@Test
	public void conn() throws UnknownHostException, IOException {
		RemoteStorage storage = RemoteStorage.open("localhost",
				Configuration.testConfig().port);
		storage.close();
	}
}
