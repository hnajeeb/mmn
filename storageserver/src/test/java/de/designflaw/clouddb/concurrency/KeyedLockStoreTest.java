package de.designflaw.clouddb.concurrency;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class KeyedLockStoreTest {
	KeyedLockStore<Integer> locks;

	@Before
	public void setup() {
		locks = new KeyedLockStore<Integer>();
	}

	@Test
	public void read() {
		locks.lockRead(1);
		assertTrue(locks.tryLockRead(1));
		assertFalse(locks.tryLockWrite(1));
		locks.freeLockRead(1);
		assertTrue(locks.tryLockRead(1));
		locks.freeLockRead(1);
	}

	@Test
	public void write() {
		locks.lockWrite(2);
		assertFalse(locks.tryLockRead(2));
		assertFalse(locks.tryLockWrite(2));
		locks.freeLockWrite(2);
		assertTrue(locks.tryLockWrite(2));
		locks.freeLockWrite(2);
	}

	@Test(timeout=300)
	public void concurrent() throws InterruptedException {
		locks.lockRead(1);
		new Thread() {
			public void run() {
				assertFalse(locks.tryLockWrite(1));
				long time = System.currentTimeMillis();
				locks.lockWrite(1);
				assertTrue(System.currentTimeMillis() - time < 200);
			}
		}.start();
		Thread.sleep(100);
		locks.freeLockRead(1);
	}
}
